# Photobox Delay Module

An Arduino Nano based trigger delay module for use in a photo box. The module takes a button input and triggers the shutter of the connected camera. The button is then on cooldown for a selected amount of time. That way humans can not spam the button and take thousands of photos.

Optionally, the module can trigger the focus frequently to prevent the camera from going into a light sleep. Some camera models need noticable time to wake up before taking a photo.

![OpenSCAD Drawing](doc/openscad_mounting_plate.png)

![Image of Plate v2](doc/photo_plate_v2.jpg)

![Wiring](doc/wiring.png)
