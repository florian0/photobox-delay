use <lib/relais.scad>
use <lib/arduino.scad>
use <lib/primitives.scad>
use <strain_relief.scad>

box_shell_width = 1;
box_width = 68;
box_depth = 68;
box_inner_width = box_width - box_shell_width * 2;
box_inner_depth = box_depth - box_shell_width * 2;

base_secure_space = 3;

base_width = box_inner_width - base_secure_space * 2;
base_depth = box_inner_depth - base_secure_space * 2;
base_height = 1;

corner_radius = 10;

hole_spacing = 36 / 2;
hole_count = 3;

$fn=20;

screw_thread_r = 1.34 / 2;

show_arduino = true;
show_box = false;
show_relais = true;



module cylinder_with_spike(h, r1, r2, spike_r, spike_l) {
    union() {
        cylinder(h, r1, r2);
        translate([0, 0, h]) cylinder(spike_l, spike_r, spike_r);
    }
}

module relais_with_stands() {
    relais_height_from_base = 3;
    
    translate([31.5, -2.5, 0])
    rotate([0, 0, 90]) {
        if (show_relais) {
            translate([0, 0, relais_height_from_base])  relais_module();
        }
        
        for (p = relais_module_drill_positions()) {
            translate([p[0], p[1], 0]) cylinder_with_spike(relais_height_from_base, 2, 1.5, 1, 4);
        }
    }
}

module cable_holder() {
    foot_width = 2.54;
    difference() {
        union() {
            cube([10, foot_width, 5]);
            hull() {
                translate([0, 0, 5]) cube([10, foot_width, 2]);
                translate([0, 0, 5 + 2]) cube([10, 7.9, 11]);
            }
        }
        gap_height = 8.1;
        translate([-1, 2, gap_height + 8])
        rotate([0, 90, 0])
        union() {        
            gap_width = 10 + 2;
            gap_opening = 6;
            gap_push = (gap_height - gap_opening) / 2;
            gap_depth_angeled = 2;
            gap_depth = 4;
            
            cube([gap_height, gap_depth, gap_width]);
            
            
            translate([0, gap_depth, 0]) linear_extrude(gap_width) polygon(points = [
                [0, 0], [gap_push, gap_depth_angeled], [gap_push + gap_opening, gap_depth_angeled], [gap_height, 0]
            ]);
        }
    }
}


hole_h = 13;


r1 = 7 / 2;
r2 = 11 / 2;
r3 = 15 / 2;
r4 = 19 / 2;


hole_list =    [
    [[box_width / 2 - hole_spacing, 0, hole_h], [90, 0, 180] ],
    [[box_width / 2, 0, hole_h], [90, 0, 180] ],
    [[box_width / 2 + hole_spacing, 0, hole_h], [90, 0, 180] ],

    [[box_width / 2 - hole_spacing, box_depth, hole_h], [90, 0, 0] ],
    [[box_width / 2, box_depth, hole_h], [90, 0, 0] ],
    [[box_width / 2 + hole_spacing, box_depth, hole_h], [90, 0, 0] ],
    
    [[0, box_depth / 2 - hole_spacing, hole_h], [90, 0, 90] ],
    [[0, box_depth / 2, hole_h], [90, 0, 90] ],
    [[0, box_depth / 2 + hole_spacing, hole_h], [90, 0, 90] ],
    
    [[box_width, box_depth / 2 - hole_spacing, hole_h], [90, 0, -90] ],
    [[box_width, box_depth / 2, hole_h], [90, 0, -90] ],
    [[box_width, box_depth / 2 + hole_spacing, hole_h], [90, 0, -90] ],
];


if (show_box) {
    difference() {
        union() {
            difference() {
                rounded_cube(box_width, box_depth, 35, corner_radius);
                translate([box_shell_width, box_shell_width, box_shell_width]) rounded_cube(box_inner_width, box_inner_depth, 35, corner_radius);
                for (p = hole_list) {
                    translate(p[0]) rotate(p[1]) translate([0, 0, -2]) cylinder(box_shell_width + 4, r3, r3);
                }
            }

            for (p = hole_list) {

                translate(p[0]) 
                rotate(p[1]) 
                difference() {
                
                    scale([1.1, 1.1, 1.1])
                    union() {
                        cylinder(3, r1, r1);
                        cylinder(2, r2, r2);
                        cylinder(1, r3, r3);
                    }
                    
                    union() {
                        cylinder(5, r1, r1);
                        cylinder(2, r2, r2);
                        cylinder(1, r3, r3);
                    }
                    
                }
            }
        }
        // Cutaway the hull
        translate([-10, -1, 1]) cube([box_width, 80, 60]);
    }

    // Nibble in the middle of the box
    translate([box_width / 2, box_depth / 2, box_shell_width]) cylinder(6, 4, 4);
}



// Base plate and cutouts
translate([box_shell_width + base_secure_space, box_shell_width + base_secure_space, 3]) {
    difference() {
        rounded_cube(base_width, base_depth, base_height, corner_radius);
        
        // Cutout for arduino programming pins
        // translate([5 + 38, 26, -3]) cube([5, 8, 5]);
        // Cutout for nibble
        translate([base_width / 2, base_depth / 2, 0]) cylinder(base_height, 5, 5);
        
        // Cutout for strain relief        
        if (1) {
            translate([base_width - 2, base_depth/2, 0]) {
                for (i = [-1, 0, 1]) {
                    translate([0, i * hole_spacing, 0]) rotate([0, 0, 90]) strain_relief_cutout(4.2, 4.2, 5);
                }
            }
            
            translate([1, base_depth/2, 0]) {
                translate([0, -1 * hole_spacing, 0]) rotate([0, 0, -90]) strain_relief_cutout(4.2, 4.2, 5);
            }
        }
    }
    

    // Components on the base plate
    translate([0, 0, base_height]) {
        
        // Arduino
        translate([5, 0, 0]) union() {
            arduino_height_from_base = 10;
        
            translate([0, base_width / 2, 0]) {
                
                translate([0, 0, arduino_height_from_base + 1.8]) {
                    if (show_arduino) {
                         rotate([180, 0, 0]) arduino_nano();
                    }
                    
                    difference() {
                        translate([15, - 9, 3]) cable_holder();
                        rotate([180, 0, 0]) arduino_nano();
                    }
                }
                
                translate([0, -18/2, 0])
                for (p = arduino_nano_pcb_drilling_positions()) {
                    translate(p) cylinder_with_spike(arduino_height_from_base, 2, 1, screw_thread_r, 4);
                }
            }
        }
        
            
        // Relais Module
        translate([11.5 + 1, 37, 0]) relais_with_stands();
        translate([11.5 + 29 + 1, 20 + 3, 0]) rotate([0, 0, 180]) relais_with_stands();
       
        
        if (1) {
            translate([base_width - 2, base_depth/2, 0]) {
                for (i = [-1, 0, 1]) {
                    translate([0, i * hole_spacing, 0]) rotate([0, 0, 90]) strain_relief(4.2, 4.2, 5);
                }
            }
            
            translate([1, base_depth/2, 0]) {
                translate([0, -1 * hole_spacing, 0]) rotate([0, 0, -90]) strain_relief(4.2, 4.2, 5);
            }
            
            
        }
    }
}

