
$fn = 20;

module groove(rotation, width, depth, groove_radius, height, ziptie_width) {
    translate([width, 0, -groove_radius + height])
    translate([0, depth / 2 - ziptie_width / 2, 0])
    rotate([-90, rotation, 0]) 
    
    intersection() {
        difference() {
            cylinder(ziptie_width, groove_radius + 20, groove_radius + 20);
            cylinder(ziptie_width, groove_radius, groove_radius);
        }
        
        cube([groove_radius + 20, groove_radius + 20, ziptie_width]);
    }
}

module strain_relief(cable_diameter, ziptie_width, standoff) {
    height = cable_diameter / 2 + 1;
    border = 2; // on each side
    groove_depth = 1;
    groove_radius = 2.5;
    width = groove_radius * 2 + cable_diameter;
    depth = ziptie_width + border * 2;
    
    translate([-width/2, 0, 0]) {
        translate([0, 0, standoff]) difference() {
            difference() {
                cube([width, depth, height]);
                translate([width / 2, -1, height]) rotate([-90, 0, 0]) cylinder(depth + 2, cable_diameter / 2, cable_diameter / 2);
            }
            
            groove(180, groove_radius, depth, groove_radius, height, ziptie_width);
            groove(-90, groove_radius + cable_diameter, depth, groove_radius, height, ziptie_width);
        }
    cube([width, depth, standoff]);    
    }
}

module strain_relief_cutout(cable_diameter, ziptie_width, standoff) {
    height = cable_diameter / 2 + 1;
    border = 2; // on each side
    groove_depth = 1;
    groove_radius = 2.5;
    width = groove_radius * 2 + cable_diameter;
    depth = ziptie_width + border * 2;
    
    
    translate([width / 2, depth / 2 - ziptie_width / 2, -3]) cube([2, ziptie_width, 5]);
    translate([-width / 2 - 2, depth / 2 - ziptie_width / 2, -3]) cube([2, ziptie_width, 5]);
}

strain_relief(4.2, 4.2, 5);

strain_relief_cutout(4.2, 4.2, 5);

