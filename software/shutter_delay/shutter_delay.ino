
// Pin configuration
const int buttonPin = 2;     // the number of the pushbutton pin
const int shutterPin = 3;    // the number of the shutter pin
const int focusPin = 4;      // the number of the focus pin


// Sleep configuration
const int sleepAfterTrigger = 5; // time in seconds to sleep before triggering again


// Wakeup timings
const bool doWakeup = true;
const int debounceTime = 10;
const int refreshEvery = 30000 / debounceTime; // interval the focus is triggered to keep the camera awake
const int focusFor = 250 / debounceTime; // interval the focus is held "down"
const int shutterTime = 2000; // interval the shutter is hold down to take a photo


void setup() {
  Serial.begin(9600);
  
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT_PULLUP);
  // initialize the shutter pin as an output
  pinMode(shutterPin, OUTPUT);

  pinMode(focusPin, OUTPUT);

  pinMode(LED_BUILTIN, OUTPUT);

  digitalWrite(shutterPin, LOW);
  digitalWrite(focusPin, LOW);
}


// Very, very simple debounced button read
// If button input is the same for given debounceTime, button is pressed.
// Returns true if button is pressed, false otherwise.
bool isButtonPressed() {
  int firstButtonState = digitalRead(buttonPin) == LOW;
  delay(debounceTime);
  int secondButtonState = digitalRead(buttonPin) == LOW;

  return firstButtonState && (firstButtonState == secondButtonState);
}

void triggerShutter() {
  Serial.println("Triggering shutter");
  digitalWrite(shutterPin, HIGH);
  delay(shutterTime); // Wait before releasing the shutter
  digitalWrite(shutterPin, LOW);
  Serial.println("Triggering done");
}

// Simple sleep function that blinks the LED to show activity
void sleepSeconds(int seconds) {
  Serial.println("Sleeping seconds");
  Serial.println(seconds);
  for (int i = 0; i < (seconds * 5); i++) {
    delay(100);
    digitalWrite(LED_BUILTIN, HIGH);
    delay(100);
    digitalWrite(LED_BUILTIN, LOW);
  }
}

void loop() {
  static int wakecounter = 0;
  
  // If button is pressed
  if (isButtonPressed()) {
    // Trigger shutter
    triggerShutter();

    // Wait before allowing any action to avoid spamming
    sleepSeconds(sleepAfterTrigger);
  }

  if (doWakeup) {
    // Wake camera up frequently
    wakecounter++;
    if (wakecounter == refreshEvery) {
      Serial.println("Waking up via focus");
      digitalWrite(focusPin, HIGH);
      digitalWrite(LED_BUILTIN, HIGH);
    } else if (wakecounter >= (refreshEvery + focusFor)) {
      digitalWrite(focusPin, LOW);
      digitalWrite(LED_BUILTIN, LOW);
      wakecounter = 0;
    }
  }
}
